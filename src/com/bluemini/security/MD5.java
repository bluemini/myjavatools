package com.bluemini.security;

import java.util.Arrays;

public class MD5
{
    public static void main(String[] args)
    {
        byte[] d = { 1, 2, 3, 4, 5 };
        d = new byte[0];
        byte[] hashout = hash(d);
        
        String hexs = "0123456789ABCDEF";
        for (byte x : hashout) {
        	// System.out.println(Integer.toHexString(x));
        	int y = (int)x & 0xFF;
        	// System.out.println(y + ", " + (y >>> 4) + ", " + (y & 0x0F));
        	System.out.print(new String(new char[] { (hexs.charAt(y >>> 4)), hexs.charAt(y & 0x0F) } ));
        }
    }
    
    public static byte[] hash(byte[] indata)
    {
        int[] K = new int[64];
        int[] s = new int[] { 7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
                              5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
                              4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
                              6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21 };
        
        for (int i=0; i < 64; i++)
        {
            K[i] = (int)(long)Math.floor(Math.abs(Math.sin(i + 1)) * Math.pow(2, 32));
            // System.out.println("K" + i + ": " + Integer.toHexString(K[i]));
        }
        
        int a0 = 0x67452301;   //A
        int b0 = 0xefcdab89;   //B
        int c0 = 0x98badcfe;   //C
        int d0 = 0x10325476;   //D
        
        byte[] data = pad(indata);
        
        int blocks = data.length / 64;
        int dTemp = 0;
        // int maxUnsignedInt = (long)Math.pow(2, 32);
        
        System.out.println("Data length: " + blocks);
        
        for (int block=0; block < blocks; block++)
        {
        	int A = a0;
        	int B = b0;
        	int C = c0;
        	int D = d0;
        	
        	System.out.println("\nInitial Valuations");
        	System.out.print("Block A: " + Integer.toHexString(A));
        	System.out.print("  Block B: " + Integer.toHexString(B));
        	System.out.print("  Block C: " + Integer.toHexString(C));
        	System.out.println("  Block D: " + Integer.toHexString(D));
        	
        	int F = 0;
        	int g = 0;
        	
        	System.out.println("\nProcessing block #" + block);
        	
        	
        	for (int i=0; i < 64; i++)
        	{
        		if (i >= 0 && i < 16)
        		{
        			F = (B & C) | ((~B) & D);
        			g = i;
        		}
        		else if (i >= 16 && i < 32)
        		{
        			F = (D & B) | ((~D) & C);
        			g = (5 * i + 1) & 0x0F; 
        		}
        		else if (i >= 32 && i < 48)
        		{
        			F = B ^ C ^ D;
        			g = (3 * i + 5) & 0x0F;
        		}
        		else if (i >= 48)
        		{
        			F = C ^ (B | (~D));
        			g = (7 * i) & 0x0F;
        		}
        		
        		int t = (int)(g + (32 * block));
        		int t_value = data[t+3] << 24 | data[t+2] << 16 | data[t+1] << 8 | (data[t] & 0xFF);
        		System.out.println("Chunk start index: " + t + "; Chunk value: " + Integer.toHexString(t_value));
        		
        		F = F & 0xFFFFFFFF;
        		
        		dTemp = D;
        		D = C;
        		C = B;
        		B = B + Integer.rotateLeft(A + F + K[i] + t_value, s[i]);
        		A = dTemp;

        		dumpABCD(A, B, C, D);
        	}
            
        	a0 += A;
        	b0 += B;
        	c0 += C;
        	d0 += D;

        	System.out.println("\nIteration " + block);
        	System.out.print("Block a0: " + Integer.toHexString(a0));
        	System.out.print("  Block b0: " + Integer.toHexString(b0));
        	System.out.print("  Block c0: " + Integer.toHexString(c0));
        	System.out.println("  Block d0: " + Integer.toHexString(d0));
        	
        }
        
        int[] res = new int[4];
        res[0] = a0;
        res[1] = b0;
        res[2] = c0;
        res[3] = d0;
        
        byte[] digest = new byte[16];
        for (int i = 0; i < 4; i++) {
        	for (int j = 0; j < 4; j++) {
                digest[(i * 4) + j] = (byte)((res[i] >> (j * 8)) & 0xFF);
        	}
        }
        
        return digest;
    }
    
    private static byte[] pad(byte[] data)
    {
        //  447 - 500 + 512 = 447 + 12
        //  447 - 400 = 47
        int currMod = (data.length * 8) % 512;
        int paddMore = 447 - currMod;
        if (paddMore < 0)
        {
            paddMore += 512;
        }
        System.out.println(data.length + ", " + paddMore);
        
        if ((paddMore + 1) % 8 != 0)
        {
            throw new IllegalArgumentException("Only working on full 8 byte values for now");
        }
        
        int deltaLength = ((1 + paddMore) / 8);
        int newLength = data.length + deltaLength + 8;
        byte[] newdata = Arrays.copyOf(data, newLength);
        for (int i = data.length; i < newLength; i++)
        {
            newdata[i] = 0;
        }
        newdata[data.length] = (byte)0x80;
        
        System.out.println("Padding length: " + (deltaLength + 8));
        
        long origLen = (long)data.length;
        for (int i=0; i<8; i++)
        {
        	newdata[newLength - 8 + i] = (byte)(origLen & 0xFF);
        	origLen = origLen >>> 8;
        	// System.out.println(newdata[newLength - 8 + i]);
        }
        
        /* for (int i = data.length; i < newLength; i++)
        {
        	System.out.println(Integer.toHexString(newdata[i]));
        } */

        return newdata;
    }
    
    private static int leftrotate(int x, int c)
    {
        // c = c % 32;
        x = x & 0xFFFFFFFF;
        int resp = (x << c) | (x >>> (32 - c));
        return resp & 0xFFFFFFFF;
    }
    
    private static void dumpABCD(int A, int B, int C, int D)
    {
    	System.out.print("\nBlock A: " + Integer.toHexString(A));
    	System.out.print("  Block B: " + Integer.toHexString(B));
    	System.out.print("  Block C: " + Integer.toHexString(C));
    	System.out.println("  Block D: " + Integer.toHexString(D));
    }
}
