package com.bluemini.nettools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class SelectListSendFile {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // create a socket, send a text file, easy!
        try
        {
            Socket s = new Socket("projects.intranet.arup.com", 80);
            OutputStream os = s.getOutputStream();
            InputStream is = s.getInputStream();
            
            String cookieData = ""; // this would be the user's authenticated cookie data
            
            os.write("POST http://projects.intranet.arup.com/index.cfm HTTP/1.1\r\n".getBytes());
            os.write("Host: projects.intranet.arup.com\r\n".getBytes());
            os.write("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0\r\n".getBytes());
            os.write("Accept: text/html,application/xhtml+xml,application/xml\r\n".getBytes());
            os.write("Content-Type: multipart/form-data, boundary=AaB03x\r\n".getBytes());
            os.write(("Cookie: " + cookieData + "\r\n").getBytes());
            os.write("Content-Length: 1270\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("--AaB03x\r\n".getBytes());
            os.write("content-disposition: form-data; name=\"nuggetFile\"; filename=\"20140509_175614_243_OBV_List.txt\"\r\n".getBytes());
            os.write("Content-Type: text/plain\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("GR List\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("*JN 215382\r\n".getBytes());
            os.write("*SO 61475\r\n".getBytes());
            os.write("*KW TRANSPORTATION PLANNING\r\n".getBytes());
            os.write("*KW TRAFFIC IMPACT ASSESSMENT\r\n".getBytes());
            os.write("*KW MULTI MODAL INTERCHANGE\r\n".getBytes());
            os.write("*TI City of Bellevue East Link B7 Revised Analysis, Bellevue, Washington, USA\r\n".getBytes());
            os.write("*NS Completed 2011\r\n".getBytes());
            os.write("ABR=502767\r\n".getBytes());
            os.write("Transportation planning, alignment and station design, public outreach, environmental analysis, cost estimating, and right-of-way assessment for the East Link project, which will extend Seattle’s Sound Transit Link system to provide light rail service across Lake Washington.\r\n".getBytes());
            os.write("*TF\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("*JN 213880\r\n".getBytes());
            os.write("*SO 59733\r\n".getBytes());
            os.write("*KW NATURAL VENTILATION\r\n".getBytes());
            os.write("*KW NATURAL VENTILATION OF OFFICES\r\n".getBytes());
            os.write("*TI Bonneville Power Administration - Transmission Services Facility, Vancouver, Washington, USA\r\n".getBytes());
            os.write("*NS Completed 2011\r\n".getBytes());
            os.write("*CR\r\n".getBytes());
            os.write("Client: Bonneville Power Administration\r\n".getBytes());
            os.write("Architect: NBBJ\r\n".getBytes());
            os.write("*TS\r\n".getBytes());
            os.write("Mechanical, electrical and plumbing engineering for two new structures on the Bonneville Power Administration’s campus. Intended to consolidate staff on campus, the buildings offer a combined 21_000ft of space and include conferencing facilities, training and dining areas.\r\n".getBytes());
            os.write("--AaB03x\r\n".getBytes());
            os.write("content-disposition: form-data; name=\"layout\"\r\n".getBytes());
            os.write("\r\n".getBytes());
            os.write("projects.do_extract_nuggets\r\n".getBytes());
            os.write("--AaB03x--\r\n".getBytes());
            
            byte[] buff = new byte[256];
            int i = 0;
            while ((i = is.read(buff)) >= 0)
            {
                System.out.print(new String(buff, 0, i));
            }
        }
        catch (UnknownHostException uhe)
        {
            System.out.println("Unknown host");
        }
        catch (IOException ioe)
        {
            System.out.println("There was a problem writing to the host");
        }
    }

}
