package com.bluemini.nettools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/***************************************************
 * A simple Java tool to send and receive ARP based packets
 * on a network. 
 * 
 * http://en.wikipedia.org/wiki/Address_Resolution_Protocol
 * Internet Protocol (IPv4) over Ethernet ARP packet
 * |==========================================================|
 * | bit  |                         |                         |
 * |offset|          0 � 7          |          8 � 15         |
 * |======|===================================================|
 * |  0   |              Hardware type (HTYPE)                |
 * |------|---------------------------------------------------|
 * |  16  |              Protocol type (PTYPE)                |
 * |------|---------------------------------------------------|
 * |  32  | Hardware address length | Protocol address length |
 * |      |         (HLEN)          |         (PLEN)          |
 * |------|---------------------------------------------------|
 * |  48  |                 Operation (OPER)                  |
 * |------|---------------------------------------------------|
 * |  64  |           Sender hardware address (SHA)           |
 * |      |                  (first 16 bits)                  |
 * |------|---------------------------------------------------|
 * |  80  |                  (next 16 bits)                   |
 * |------|---------------------------------------------------|
 * |  96  |                  (last 16 bits)                   |
 * |------|---------------------------------------------------|
 * |  112 |           Sender protocol address (SPA)           | 
 * |      |                  (first 16 bits)                  |
 * |------|---------------------------------------------------|
 * |  128 |                  (last 16 bits)                   |
 * |------|---------------------------------------------------|
 * |  144 |           Target hardware address (THA)           |
 * |      |                  (first 16 bits)                  |
 * |------|---------------------------------------------------|
 * |  160 |                  (next 16 bits)                   |
 * |------|---------------------------------------------------|
 * |  176 |                  (last 16 bits)                   |
 * |------|---------------------------------------------------|
 * |  192 |           Target protocol address (TPA)           |
 * |      |                  (first 16 bits)                  |
 * |------|---------------------------------------------------|
 * |  208 |                  (last 16 bits)                   |
 * |======|===================================================|
 * 
 * @author nick.harvey
 *
 */

public class Arp {
	
	private ByteArrayOutputStream arpRequest;
	private NetworkInterface localNif;
	private InetAddress localAddress;
	
	public static void main(String[] args) {
		System.out.println("Yes");
		Arp arp = new Arp();
		byte[] remoteIP = new byte[4];
		remoteIP[0] = 10;
		remoteIP[1] = 11;
		remoteIP[2] = 15;
		remoteIP[3] = 4;
		String mac = arp.resolve(remoteIP);
	}
	
	private String resolve(byte[] ipaddress) {
		byte[] localMac;
		byte[] localIP;
		byte[] remoteMac = new byte[6];
		
		try {
			Enumeration<NetworkInterface> nifs = NetworkInterface.getNetworkInterfaces();
			if (nifs.hasMoreElements()) { 
				localNif = nifs.nextElement(); 
			} else {
				return "";
			}
			localMac = localNif.getHardwareAddress();
			localIP = InetAddress.getLocalHost().getAddress();
		} catch (SocketException se) {
			se.printStackTrace();
			return "";
		} catch (UnknownHostException uhe) {
			uhe.printStackTrace();
			return "";
		}
		
		// build remoteMac
		for (int i=0; i<6; i++) {
			remoteMac[i] = 0;
		}

		ByteArrayOutputStream requestStream = buildArpRequest(ipaddress, remoteMac, localIP, localMac);
		sendRequest(requestStream);
		
		return "";
	}
	
	private ByteArrayOutputStream buildArpRequest(byte[] remoteIP, byte[] remoteMac, byte[] localIP, byte[] localMac) {
		arpRequest = new ByteArrayOutputStream(28);
		// ethernet = 0x0001
		arpRequest.write(0);
		arpRequest.write(1);
		// IP = 0x0800
		arpRequest.write(0x08);
		arpRequest.write(0x00);
		// hardware size = 6
		arpRequest.write(6);
		// protocol size = 4
		arpRequest.write(4);
		// set the operation to 0x0002
		arpRequest.write(0x00);
		arpRequest.write(0x02);
		// set our local MAC/IP
		arpRequest.write(localMac, 0, localMac.length);
		arpRequest.write(localIP, 0, localIP.length);
		// set our remote question..
		arpRequest.write(remoteMac, 0, remoteMac.length);
		arpRequest.write(remoteIP, 0, remoteIP.length);
		
		return arpRequest;
	}
	
	private void sendRequest(ByteArrayOutputStream requestStream) {
	    DatagramSocket s = null;
		try
		{
	        s = new DatagramSocket();
	        byte[] packet = requestStream.toByteArray();
	        DatagramPacket p = new DatagramPacket(packet, packet.length);
	        s.send(p);
		}
		catch (IOException ioe)
		{
		    System.out.println("Problem sending packet");
		}
		finally
		{
		    if (s != null) s.close();
		}
	}

}
